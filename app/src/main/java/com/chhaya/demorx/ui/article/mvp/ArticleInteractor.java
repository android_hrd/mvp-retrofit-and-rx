package com.chhaya.demorx.ui.article.mvp;

import android.util.Log;

import com.chhaya.demorx.callback.InteractorCallback;
import com.chhaya.demorx.data.api.ServiceGenerator;
import com.chhaya.demorx.data.api.response.ArticleResponse;
import com.chhaya.demorx.data.api.service.ArticleService;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleInteractor implements ArticleMvp.Interactor {

    private ArticleService service;
    private CompositeDisposable disposable;

    public ArticleInteractor() {
        service = ServiceGenerator.createService(ArticleService.class);
    }

    @Override
    public void findOne(int id, final InteractorCallback<ArticleResponse> callback) {
        Call<ArticleResponse> response = service.findOne(id);
        response.enqueue(new Callback<ArticleResponse>() {
            @Override
            public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<ArticleResponse> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    @Override
    public void findById(int id, final InteractorCallback<ArticleResponse> callback) {
        Log.d("TAG", "TEST");
        Single<ArticleResponse> response = service.findById(id);
        disposable = new CompositeDisposable();
        disposable.add(response.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ArticleResponse>() {
                    @Override
                    public void onSuccess(ArticleResponse response) {
                        callback.onSuccess(response);
                        Log.d("TAG", response.toString());
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(e.getMessage());
                        Log.d("TAG", e.getMessage());
                    }
                }));

    }

    @Override
    public void onDestroy() {
        disposable.clear();
    }
}
