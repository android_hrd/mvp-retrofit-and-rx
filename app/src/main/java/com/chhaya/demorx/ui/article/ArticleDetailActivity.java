package com.chhaya.demorx.ui.article;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chhaya.demorx.R;
import com.chhaya.demorx.data.api.response.ArticleResponse;
import com.chhaya.demorx.ui.article.mvp.ArticleMvp;
import com.chhaya.demorx.ui.article.mvp.ArticlePresenter;

public class ArticleDetailActivity extends AppCompatActivity implements ArticleMvp.View {

    private ImageView imageArticle;
    private TextView textTitle, textDesc;
    private ArticlePresenter articlePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_detail);

        imageArticle = findViewById(R.id.image_article);
        textTitle = findViewById(R.id.text_title);
        textDesc = findViewById(R.id.text_desc);

        articlePresenter = new ArticlePresenter();
        articlePresenter.setView(this);
        //articlePresenter.onFindOne(120430);
        articlePresenter.onFindById(120430);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void onResponseArticle(ArticleResponse response) {
        Glide.with(this).load(response.getData().getImage()).into(imageArticle);
        textTitle.setText(response.getData().getTitle());
        textDesc.setText(response.getData().getDescription());
    }

    @Override
    public void showMessage(String message) {

    }
}
