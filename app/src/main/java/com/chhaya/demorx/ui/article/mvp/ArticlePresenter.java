package com.chhaya.demorx.ui.article.mvp;

import android.util.Log;

import com.chhaya.demorx.callback.InteractorCallback;
import com.chhaya.demorx.data.api.response.ArticleResponse;

public class ArticlePresenter implements ArticleMvp.Presenter {

    private ArticleMvp.View view;
    private ArticleInteractor interactor;

    public ArticlePresenter() {
        interactor = new ArticleInteractor();
    }

    @Override
    public void onFindOne(int id) {
        interactor.findOne(id, new InteractorCallback<ArticleResponse>() {
            @Override
            public void onSuccess(ArticleResponse response) {
                view.onResponseArticle(response);
            }

            @Override
            public void onError(String message) {
                view.showMessage(message);
            }
        });
    }

    @Override
    public void onFindById(int id) {
        interactor.findById(id, new InteractorCallback<ArticleResponse>() {
            @Override
            public void onSuccess(ArticleResponse response) {
                view.onResponseArticle(response);
                Log.d("TAG", response.toString());
                interactor.onDestroy();
            }

            @Override
            public void onError(String message) {
                Log.d("TAG", message);
                view.showMessage(message);
            }
        });
    }

    @Override
    public void setView(ArticleMvp.View view) {
        this.view = view;
    }
}
