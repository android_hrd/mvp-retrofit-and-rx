package com.chhaya.demorx.ui.article.mvp;

import com.chhaya.demorx.callback.InteractorCallback;
import com.chhaya.demorx.data.api.response.ArticleResponse;

public interface ArticleMvp {

    interface View {
        void showLoading();
        void hideLoading();
        void onResponseArticle(ArticleResponse response);
        void showMessage(String message);
    }

    interface Presenter {
        void onFindOne(int id);
        void onFindById(int id);
        void setView(View view);
    }

    interface Interactor {
        void findOne(int id, InteractorCallback<ArticleResponse> callback);
        void findById(int id, InteractorCallback<ArticleResponse> callback);
        void onDestroy();
    }

}
