package com.chhaya.demorx.callback;

import com.chhaya.demorx.data.api.response.ArticleResponse;

public interface InteractorCallback<T> {
    void onSuccess(T response);
    void onError(String message);
}
