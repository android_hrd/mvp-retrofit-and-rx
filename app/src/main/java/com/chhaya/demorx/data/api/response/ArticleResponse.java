package com.chhaya.demorx.data.api.response;

import com.chhaya.demorx.data.api.entity.ArticleEntity;
import com.google.gson.annotations.SerializedName;

public class ArticleResponse {

    @SerializedName("DATA")
    private ArticleEntity data;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("CODE")
    private String code;

    public ArticleEntity getData() {
        return data;
    }

    public void setData(ArticleEntity data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
