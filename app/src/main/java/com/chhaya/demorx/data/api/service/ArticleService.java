package com.chhaya.demorx.data.api.service;

import com.chhaya.demorx.data.api.response.ArticleResponse;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ArticleService {

    @GET("v1/api/articles/{id}")
    Call<ArticleResponse> findOne(@Path("id") int id);

    @GET("v1/api/articles/{id}")
    Single<ArticleResponse> findById(@Path("id") int id);

    @GET("v1/api/articles")
    Observable<ArticleResponse> findAll();


}
